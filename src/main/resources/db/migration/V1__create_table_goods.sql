create table if not exists goods
(
    id    bigint primary key auto_increment,
    name  varchar(128)   not null,
    price decimal(10, 2) not null,
    unit  varchar(64)    not null,
    image varchar(256)
) engine = InnoDB
  default charset = utf8;


