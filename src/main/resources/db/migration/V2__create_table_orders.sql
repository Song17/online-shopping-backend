create table if not exists orders
(
    id bigint primary key auto_increment,
    user varchar(64) default 'root',
    goods_id bigint not null ,
    quantity int default 1,
    foreign key (goods_id) references goods (id)
);

