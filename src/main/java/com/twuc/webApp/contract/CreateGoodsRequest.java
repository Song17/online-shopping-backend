package com.twuc.webApp.contract;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CreateGoodsRequest {
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    private double price;
    @NotNull
    @NotEmpty
    private String unit;
    private String image;

    public String getName() {
        return name;
    }

    public CreateGoodsRequest(String name, double price, String unit, String immage) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = immage;
    }

    public CreateGoodsRequest() {
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
