package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Order;

public class GetOrderResponse {
    private Long goodsId;
    private String user;
    private String name;
    private Double price;
    private String unit;
    private int quantity;

    public GetOrderResponse(Order order) {
        this.user = order.getUser();
        this.goodsId = order.getGoods().getId();
        this.name = order.getGoods().getName();
        this.price = order.getGoods().getPrice();
        this.unit = order.getGoods().getUnit();
        this.quantity = order.getQuantity();
    }

    public GetOrderResponse() {
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public String getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public int getQuantity() {
        return quantity;
    }
}
