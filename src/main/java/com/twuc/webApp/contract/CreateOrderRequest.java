package com.twuc.webApp.contract;

public class CreateOrderRequest {
    private String user;
    private Long goodsId;
    private int quantity;

    public CreateOrderRequest(String user, Long goodsId, int quantity) {
        this.user = user;
        this.goodsId = goodsId;
        this.quantity = quantity;
    }

    public CreateOrderRequest() {
    }

    public String getUser() {
        return user;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public int getQuantity() {
        return quantity;
    }
}
