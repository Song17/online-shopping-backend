package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String user;

    @ManyToOne
    @JoinColumn
    private Goods goods;

    private int quantity;

    public Order(Goods goods) {
        this(goods, 1);
    }

    public Long getId() {
        return id;
    }

    public Order(Goods goods, int quantity) {
        this.goods = goods;
        this.quantity = quantity;
    }

    public Order(String user, Goods goods, int quantity) {
        this.user = user;
        this.goods = goods;
        this.quantity = quantity;
    }

    public Order() {
    }

    public Goods getGoods() {
        return goods;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUser() {
        return user;
    }
}
