package com.twuc.webApp.domain;

import com.twuc.webApp.contract.CreateGoodsRequest;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotEmpty
    private String name;

    @NotNull
    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    @NotEmpty
    private String unit;
    private String image;

    public Goods(String name, Double price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Goods(CreateGoodsRequest goodsRequest) {
        this.name = goodsRequest.getName();
        this.price = goodsRequest.getPrice();
        this.unit = goodsRequest.getUnit();
        this.image = goodsRequest.getImage();
    }

    public Goods() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Goods goods = (Goods) o;
        return Objects.equals(id, goods.id) &&
                Objects.equals(name, goods.name) &&
                Objects.equals(price, goods.price) &&
                Objects.equals(unit, goods.unit) &&
                Objects.equals(image, goods.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, unit, image);
    }

    public void setName(String name) {
        this.name = name;
    }
}

