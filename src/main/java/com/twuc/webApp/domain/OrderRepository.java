package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    public Optional<Order> findByUserAndGoodsId(String user, Long goodsId);

    public void deleteByUserAndGoodsId(String user, Long goodsId);
}
