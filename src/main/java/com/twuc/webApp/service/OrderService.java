package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateOrderRequest;
import com.twuc.webApp.contract.GetOrderResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.GoodsRepository;
import com.twuc.webApp.domain.Order;
import com.twuc.webApp.domain.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private OrderRepository orderRepository;

    public Order save(CreateOrderRequest request) {
        Goods goods = goodsRepository.findById(request.getGoodsId()).get();
        Optional<Order> optional = orderRepository
                .findByUserAndGoodsId(request.getUser(), request.getGoodsId());
        Order order = optional.orElse(new Order());
        order.setUser(request.getUser());
        order.setGoods(goods);
        order.setQuantity(request.getQuantity());
        return orderRepository.save(order);
    }

    public List<GetOrderResponse> getAllOrders() {
        return orderRepository.findAll()
                .stream()
                .map(order -> new GetOrderResponse(order))
                .collect(Collectors.toList());
    }

    public void deleteOrderByUserAndGoodsId(String user, Long goodsId) {
        orderRepository.deleteByUserAndGoodsId(user, goodsId);
    }
}
