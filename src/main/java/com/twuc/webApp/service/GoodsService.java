package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.GoodsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsService {
    @Autowired
    private GoodsRepository repository;

    public Goods save(CreateGoodsRequest goodsRequest) {
        Goods goods = new Goods(goodsRequest);
        return repository.save(goods);
    }

    public Goods getGoodsById(Long id) {
        return repository.findById(id).get();
    }

    public List<Goods> getAllGoods() {
        return repository.findAll();
    }
}
