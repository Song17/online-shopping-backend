package com.twuc.webApp.controller;

import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/goods")
@CrossOrigin
public class GoodsController {
    @Autowired
    private GoodsService service;

    @PostMapping
    public ResponseEntity createGoods(@RequestBody CreateGoodsRequest goods) {
        Goods savedGoods = service.save(goods);
        System.out.println(savedGoods.getName());
        return ResponseEntity.status(HttpStatus.CREATED)
                .location(linkTo(methodOn(GoodsController.class)
                        .getGoodsById(savedGoods.getId())).toUri())
                .build();
    }

    @GetMapping("/{id}")
    public ResponseEntity getGoodsById(@PathVariable Long id) {
        Goods goods = service.getGoodsById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(goods);
    }

    @GetMapping
    public ResponseEntity getAllGoods() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.getAllGoods());
    }

}
