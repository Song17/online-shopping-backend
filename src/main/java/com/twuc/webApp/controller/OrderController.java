package com.twuc.webApp.controller;

import com.twuc.webApp.contract.CreateOrderRequest;
import com.twuc.webApp.domain.Order;
import com.twuc.webApp.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin
public class OrderController {
    @Autowired
    private OrderService service;

    @PutMapping
    public ResponseEntity updateOrderByUserAndGoodsId(
            @RequestParam(defaultValue = "root") String user,
            @RequestParam Long goodsId,
            @RequestParam(defaultValue = "1") int quantity
    ) {
        Order order = service.save(new CreateOrderRequest(user, goodsId, quantity));
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }

    @GetMapping
    public ResponseEntity getAllOrders() {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(service.getAllOrders());
    }

    @DeleteMapping
    @Transactional
    public ResponseEntity deleteOrderByUserAndGoodsId(
            @RequestParam String user,
            @RequestParam Long goodsId
    ) {
        service.deleteOrderByUserAndGoodsId(user, goodsId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
