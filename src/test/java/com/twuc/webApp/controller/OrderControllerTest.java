package com.twuc.webApp.controller;

import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.service.GoodsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OrderControllerTest extends ControllerTestBase {
    @Autowired
    private GoodsService goodsService;

    private Goods goods;

    @BeforeEach
    void setUp() {
        goods = goodsService.save(new CreateGoodsRequest("雪碧", 2.5, "瓶", null));
    }

    @Test
    void should_save_order() throws Exception {
        getMockMvc().perform(put("/api/orders")
                .param("user", "root")
                .param("goodsId", goods.getId().toString())
                .param("quantity", "1"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_404_when_goods_not_exist() throws Exception {
        getMockMvc().perform(put("/api/orders")
                .param("goodsId", "101")
                .param("quantity", "1"))
                .andExpect(status().is(404));
    }

    @Test
    void should_get_all_orders() throws Exception {
        getMockMvc().perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value("0"));
    }
}
