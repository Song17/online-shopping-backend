package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.CreateGoodsRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GoodsControllerTest extends ControllerTestBase {
    @Autowired
    private GoodsController controller;

    @Test
    void should_return_201() throws Exception {
        CreateGoodsRequest goodsRequest = new CreateGoodsRequest("雪碧", 4.0, "瓶", null);
        getMockMvc().perform(post("/api/goods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper()
                        .writeValueAsString(goodsRequest)))
                .andExpect(status().is(201))
                .andExpect(header().string("location", "http://localhost/api/goods/2"));
    }

    @Test
    void should_return_400_and_error_message_when_create_invalid_goods() throws Exception {
        CreateGoodsRequest goodsRequest = new CreateGoodsRequest(null, 4, "瓶", null);
        getMockMvc().perform(post("/api/goods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper()
                        .writeValueAsString(goodsRequest)))
                .andExpect(status().is(400))
                .andExpect(content().string("Invalid Goods!"));
    }

    @Test
    void should_get_goods_by_id() throws Exception {
        getMockMvc().perform(get("/api/goods/1"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.name").value("雪碧"))
                .andExpect(jsonPath("$.price").value(4))
                .andExpect(jsonPath("$.unit").value("瓶"));
    }

    @Test
    void should_return_404_when_goods_not_exist() throws Exception {
        getMockMvc().perform(get("/api/goods/2"))
                .andExpect(status().is(404));
    }

    @Test
    void should_get_all_goods() throws Exception {
        controller.createGoods(new CreateGoodsRequest("雪碧", 4.0, "瓶", null));

        getMockMvc().perform(get("/api/goods"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.length()").value("1"))
                .andExpect(jsonPath("$.[0].name").value("雪碧"))
                .andExpect(jsonPath("$.[0].price").value(4))
                .andExpect(jsonPath("$.[0].unit").value("瓶"));
    }
}
