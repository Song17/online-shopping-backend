package com.twuc.webApp.domain;

import com.twuc.webApp.ClosureValue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GoodsRepositoryTest extends JpaTestBase {
    @Autowired
    private GoodsRepository repository;

    @Test
    void should_create_goods() {
        ClosureValue<Long> id = new ClosureValue<>();
        Goods goods = new Goods("雪碧", 2.5, "瓶");
        flushAndClear(em -> {
            Goods savedGoods = repository.save(goods);
            id.setValue(savedGoods.getId());

        });

        assertEquals(goods, repository.findById(goods.getId()).get());
    }

    @Test
    void should_get_all_goods() {
        Goods goods = repository.save(new Goods("雪碧", 2.5, "瓶"));
        List<Goods> allGoods = repository.findAll();

        assertEquals(1, allGoods.size());
        assertEquals(goods, allGoods.get(0));
    }
}
