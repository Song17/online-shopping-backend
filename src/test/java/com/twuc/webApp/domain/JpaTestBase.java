package com.twuc.webApp.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.function.Consumer;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public abstract class JpaTestBase {
    @Autowired
    private EntityManager entityManager;

    protected void clear(Runnable runnable) {
        runnable.run();
        entityManager.clear();
    }

    protected void flushAndClear(Consumer<EntityManager> consumer) {
        try {
            consumer.accept(entityManager);
        } finally {
            entityManager.flush();
            entityManager.clear();
        }
    }
}
