package com.twuc.webApp.domain;

import com.twuc.webApp.ClosureValue;
import com.twuc.webApp.contract.CreateGoodsRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class OrderRepositoryTest extends JpaTestBase {
    @Autowired
    private OrderRepository repository;
    @Autowired
    private GoodsRepository goodsRepository;
    private Goods savedGoods;

    @BeforeEach
    void setUp() {
        flushAndClear(em ->
                savedGoods = goodsRepository
                        .save(new Goods(new CreateGoodsRequest("雪碧", 4.0, "瓶", null))));
    }

    @Test
    void should_create_order() {
        ClosureValue<Long> orderId = new ClosureValue<>();

        flushAndClear(em -> {
            Order order = repository.save(new Order(savedGoods));
            orderId.setValue(order.getId());

        });

        Optional<Order> optional = repository.findById(orderId.getValue());

        assertTrue(optional.isPresent());
        assertEquals(savedGoods, optional.get().getGoods());
    }

}
