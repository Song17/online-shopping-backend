package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.contract.CreateOrderRequest;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class OrderServiceTest {
    @Autowired
    private OrderService orderService;
    @Autowired
    private GoodsService goodsService;

    private Goods goods;

    @BeforeEach
    void setUp() {
        goods = goodsService.save(new CreateGoodsRequest("雪碧", 2.5, "瓶", null));
    }

    @Test
    void should_save_order() {
        Order order = orderService.save(new CreateOrderRequest("root", goods.getId(), 1));

        assertNotNull(order);
        assertEquals(goods, order.getGoods());
    }

    @Test
    void should_update_order_if_order_exist() {
        Order order = orderService.save(new CreateOrderRequest("root", goods.getId(), 1));
        Order updatedOrder = orderService.save(new CreateOrderRequest("root", goods.getId(), 2));

        assertEquals(order.getId(), updatedOrder.getId());
    }
}
