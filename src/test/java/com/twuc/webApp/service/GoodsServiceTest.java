package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.domain.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class GoodsServiceTest {
    @Autowired
    private GoodsService service;

    @Test
    void should_create_goods() {
        CreateGoodsRequest goodsRequest = new CreateGoodsRequest("雪碧", 2.5, "瓶", null);
        Goods goods = service.save(goodsRequest);
        Goods savedGoods = service.getGoodsById(goods.getId());

        assertNotNull(goods);
        assertEquals(goods, savedGoods);
    }

    @Test
    void should_get_all_goods() {
        Goods goods = service.save(new CreateGoodsRequest("雪碧", 2.5, "瓶", null));
        List<Goods> allGoods = service.getAllGoods();

        assertEquals(1, allGoods.size());
        assertEquals(goods, allGoods.get(0));
    }
}
